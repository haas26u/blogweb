<?php
/**
 * Created by PhpStorm.
 * User: lucashaas
 * Date: 12/11/14
 * Time: 09:02
 */

require_once 'vendor/autoload.php';
use \blogapp\model\Categorie;
\conf\ConfEloquent::configurer('db.config.ini');

//echo "Test de l'index dans iutserver/projet".'</br>';

//echo $_SERVER['REQUEST_URI'].'</br>';
//echo $_SERVER['SCRIPT_NAME'].'</br>';

$h = new \picof\utils\HttpRequest();

$app = new \picof\dispatch\Dispatcher($h);

$app->addRoute('index.php','\blogapp\control\BlogController', 'listerBillets');
$app->addRoute('blog/list', '\blogapp\control\BlogController', 'listerBillets');
$app->addRoute('blog/billet', '\blogapp\control\BlogController', 'afficherBillet');
$app->addRoute('blog/liscat', '\blogapp\control\BlogController', 'listerCategories');
$app->addRoute('blog/cat', '\blogapp\control\BlogController', 'listerBilletsCategorie');
$app->addRoute('admin/panel', '\blogapp\control\AdminController', 'accederPanel');
$app->addRoute('admin/billet/add', '\blogapp\control\AdminController', 'ajouterMessageBillet');
$app->addRoute('admin/billet/save', '\blogapp\control\AdminController', 'envoyerDonneesBillet');
$app->addRoute('admin/billet/remove/list', '\blogapp\control\AdminController', 'supprimerBillet');
$app->addRoute('admin/billet/remove/save', '\blogapp\control\AdminController', 'supprimerDonneesBillet');
$app->addRoute('admin/cat/add', '\blogapp\control\AdminController', 'ajouterCategorie');
$app->addRoute('admin/cat/save', '\blogapp\control\AdminController', 'envoyerDonneesCategorie');
$app->addRoute('admin/cat/remove/list', '\blogapp\control\AdminController', 'supprimerCategorie');
$app->addRoute('admin/cat/remove/save', '\blogapp\control\AdminController', 'supprimerDonneesCategorie');
$app->addRoute('admin/register/add', '\blogapp\control\AdminController', 'ajouterUtilisateur');
$app->addRoute('admin/register/save', '\blogapp\control\AdminController', 'creerUtilisateur');

$app->dispatch();




