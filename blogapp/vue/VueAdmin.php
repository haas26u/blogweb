<?php
/**
 * Created by PhpStorm.
 * User: lucashaas
 * Date: 12/12/14
 * Time: 08:36
 */

namespace blogapp\vue;


class VueAdmin
{

    private $tableauBillets;

    public function __construct()
    {

    }

    /**
     * methode publique qui gere l'affichage general
     * si le parametre vaut 1, cela affiche les details d'un billet
     * si le parametre vaut 2, cela affiche une liste de billets
     */
    public function render($selecteur)
    {
        $d = \picof\dispatch\Dispatcher::getInstance();
        $r = $d->getRequest();
        $route = $r->getRouteInfo();

        $f = <<<END
        <!DOCTYPE html>
            <html>
                <head>
                    <meta charset="utf-8" />
                    <title>Les Gremlins montrent leur culture</title>
                    <link href="/~lucashaas/iutserver/projet/css/style-admin.css" rel="stylesheet" type="text/css" />
                </head>

                <body>
                    <header>
                        <img src="/~lucashaas/iutserver/projet/css/ressources/gremlins_movie.jpg" alt="Image de Gremlins" id="logo" />
                        <p class="titre-page">Panneau d'administration</p>
                    </header>
                    <section>

END;
        if ($selecteur == 0) {
            $f=$f. $this->afficherPanel();
        } else if ($selecteur == 1) {
            $f=$f. $this->creerBillet();
        } else if ($selecteur == 2) {
            $f=$f. $this->creerCategorie();
        } else if ($selecteur == 3) {
            $f=$f. $this->indicationBillet();
        } else if ($selecteur == 4) {
            $f=$f. $this->indicationCategorie();
        } else if ($selecteur == 5) {
            $f=$f. $this->creerUtilisateur();
        } else if ($selecteur == 6) {
            $f=$f. $this->supprimerBillet();
        } else if ($selecteur == 7) {
            $f=$f. $this->indicationSupprBillet();
        } else if ($selecteur == 8) {
            $f=$f. $this->supprimerCategorie();
        } else if ($selecteur == 9) {
            $f=$f. $this->indicationSupprCategorie();
        }

        $f = $f.<<<END
                    </section>
                    <footer>
                        <ul id="liens-footer">
				            <li class="first">
END;
        $f=$f.                  "<a href=\"" . $route . "/index.php"."\">"."Accueil</a>";
        $f=$f.<<<END
				            </li>
			            </ul>
		                <p> &copy; 2014-2015 Lucas HAAS et Theodore LAMBOLEZ. Tous droits reserves.</p>
                    </footer>
                </body>
            </html>
END;

        print $f;
    }

    public function afficherPanel() {
        $d = \picof\dispatch\Dispatcher::getInstance();
        $r = $d->getRequest();
        $route = $r->getRouteInfo();
        $f="<fieldset>"."<legend>Administration des billets</legend>";
        $f=$f."<p>"."<a href=\"" . $route . "/admin/billet/add"."\">"."Ajouter un billet</a>"."</p>";
        $f=$f."<p>"."<a href=\"" . $route . "/admin/billet/remove/list"."\">"."Supprimer un billet</a>"."</p>";
        $f=$f."</fieldset>";
        $f=$f."<fieldset>"."<legend>Administration des categories</legend>";
        $f=$f."<p>"."<a href=\"" . $route . "/admin/cat/add"."\">"."Ajouter une categorie</a>"."</p>";
        $f=$f."<p>"."<a href=\"" . $route . "/admin/cat/remove/list"."\">"."Supprimer une categorie</a>"."</p>";
        $f=$f."</fieldset>";
        return $f;
    }

    public function creerBillet() {
        $d = \picof\dispatch\Dispatcher::getInstance();
        $r = $d->getRequest();
        $route = $r->getRouteInfo();
        $f="<h2>Creation d'un billet</h2>";
        $f=$f.'<div class="creation">';
        $f=$f.'<form id="fcb" method="POST" action="'.$route. "/admin/billet/save" .'">';
        $f=$f.<<<END
                <p><label for="fcb_titre">* Titre de l'article: </label>
                <input type="text" id="fcb_titre" name="titre_article" required></p>
                <p><label for="fcb_corps">* Corps de l'article: </label>
                <textarea id="fcb_corps" name="corps_article" rows="10" required> </textarea>
END;
        $f=$f.  '<p><label for="fcb_categorie">* Nom de la catégorie: </label>';
        $f=$f.  '<select name="select-categorie">';
        $r = \blogapp\model\Categorie::all();
        foreach ($r as $v) {
            $f=$f. '<option value ="'.$v->titre.'">'.$v->titre."</option>";
        }
        $f=$f.  '</select>';
        $f=$f. '<p class="champ-obl">Les champs indiques par une * sont obligatoires.</p>';
        $f=$f.  '<button type="submit" name="valider_creation" value="valid_fcb">Publier</button>';
        $f=$f."</form>";
        $f=$f."</div>";
        return $f;
    }

    public function creerCategorie() {
        $d = \picof\dispatch\Dispatcher::getInstance();
        $r = $d->getRequest();
        $route = $r->getRouteInfo();
        $f="<h2>Creation d'une categorie</h2>";
        $f=$f.'<div class="creation">';
        $f=$f.'<form id="fcc" method="POST" action="'.$route. "/admin/cat/save" .'">';
        $f=$f.<<<END
                <p><label for="fcc_titre">* Titre de la categorie: </label>
                <input type="text" id="fcc_titre" name="titre_categorie" required></p>
                <p><label for="fcc_description">* Description de la categorie: </label>
                <input type="text" id="fcc_message" name="description_categorie" required></p>
                <p class="champ-obl">Les champs indiques par une * sont obligatoires.</p>
                <button type="submit" name="valider_creation" value="valid_fcc">Publier</button>
            </form>
            </div>
END;
        return $f;
    }

    public function indicationBillet() {
        $d = \picof\dispatch\Dispatcher::getInstance();
        $r = $d->getRequest();
        $route = $r->getRouteInfo();
        $f = '<div>';
        $f=$f. '<h3>'."Le billet a ete cree avec succes"."</h3>";
        $f=$f. '<p class="retour-accueil">'."<a href=\"".$route."/blog/index.php"."\">"."Retouner a l'accueil"."</a></p>";
        $f = $f."</div>";
        return $f;
    }

    public function indicationCategorie() {
        $d = \picof\dispatch\Dispatcher::getInstance();
        $r = $d->getRequest();
        $route = $r->getRouteInfo();
        $f = '<div>';
        $f=$f. '<h3>'."La categorie a ete creee avec succes"."</h3>";
        $f=$f. '<p class="retour-accueil">'."<a href=\"".$route."/blog/index.php"."\">"."Retouner a l'accueil"."</a></p>";
        $f = $f."</div>";
        return $f;
    }

    public function creerUtilisateur() {
        $d = \picof\dispatch\Dispatcher::getInstance();
        $r = $d->getRequest();
        $route = $r->getRouteInfo();
        $f="<h2>Creation d'un compte</h2>";
        $f=$f.'<div class="creation">';
        $f=$f.'<form id="fcu" method="POST" action="'.$route. "/admin/register/save" .'">';
        $f=$f.<<<END
                <p><label for="fcu_nom">* Nom d'utilisateur: </label>
                <input type="text" id="fcu_nom" name="nom_utilisateur" required></p>
                <p><label for="fcu_nom">* E-mail: </label>
                <input type="mail" id="fcu_mail" name="mail_utilisateur" required></p>
                <p><label for="fcu_mdp">* Mot de passe: </label>
                <input type="password" id="fcu_mdp" name="mdp_utilisateur" required></p>
                <p class="champ-obl">Les champs indiques par une * sont obligatoires.</p>
                <button type="submit" name="valider_creation" value="valid_fcu">Valider</button>
            </form>
            </div>
END;
        return $f;
    }

    public function supprimerBillet() {
        $d = \picof\dispatch\Dispatcher::getInstance();
        $r = $d->getRequest();
        $route = $r->getRouteInfo();
        $f="<h2>Suppression d'un billet</h2>";
        $f=$f.'<div class="suppression">';
        $f=$f.'<form id="fsb" method="POST" action="'.$route. "/admin/billet/remove/save" .'">';
        $f=$f.'<p>Cochez les billets que vous voulez supprimer :<br />';
        $f=$f.'</p>';
        $r = \blogapp\model\Billet::all();
        foreach ($r as $v) {
            $f=$f.'<input type="checkbox" name="billet[]" value="'.$v->id.'" /> <label for="fsb_nom">'.$v->titre.'</label><br />';
        }
        $f=$f.  '<button type="submit" name="valider_choix" value="valid_fsb">Supprimer</button>';
        $f=$f."</form>";
        $f=$f."</div>";
        return $f;
    }

    public function indicationSupprBillet() {
        $d = \picof\dispatch\Dispatcher::getInstance();
        $r = $d->getRequest();
        $route = $r->getRouteInfo();
        $f = '<div>';
        $f=$f. '<h3>'."Le(s) billet(s) a(ont) ete supprime(s) avec succes"."</h3>";
        $f=$f. '<p class="retour-accueil">'."<a href=\"".$route."/blog/index.php"."\">"."Retouner a l'accueil"."</a></p>";
        $f = $f."</div>";
        return $f;
    }

    public function supprimerCategorie() {
        $d = \picof\dispatch\Dispatcher::getInstance();
        $r = $d->getRequest();
        $route = $r->getRouteInfo();
        $f="<h2>Suppression d'une categorie</h2>";
        $f=$f.'<div class="suppression">';
        $f=$f.'<form id="fsc" method="POST" action="'.$route. "/admin/cat/remove/save" .'">';
        $f=$f.'<p>Cochez les categories que vous voulez supprimer :<br />';
        $f=$f.'</p>';
        $r = \blogapp\model\Categorie::all();
        foreach ($r as $v) {
            $f=$f.'<input type="checkbox" name="categorie[]" value="'.$v->id.'" /> <label for="fsc_nom">'.$v->titre.'</label><br />';
        }
        $f=$f.  '<button type="submit" name="valider_choix" value="valid_fsc">Supprimer</button>';
        $f=$f."</form>";
        $f=$f."</div>";
        return $f;
    }

    public function indicationSupprCategorie() {
        $d = \picof\dispatch\Dispatcher::getInstance();
        $r = $d->getRequest();
        $route = $r->getRouteInfo();
        $f = '<div>';
        $f=$f. '<h3>'."La(es) categorie(s) a(ont) ete supprimee(s) avec succes"."</h3>";
        $f=$f. '<p class="retour-accueil">'."<a href=\"".$route."/blog/index.php"."\">"."Retouner a l'accueil"."</a></p>";
        $f = $f."</div>";
        return $f;
    }

}
