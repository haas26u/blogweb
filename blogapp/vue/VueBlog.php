<?php
/**
 * Created by PhpStorm.
 * User: lucashaas
 * Date: 12/12/14
 * Time: 08:36
 */

namespace blogapp\vue;


class VueBlog
{

    private $tableauBillets;

    public function __construct($tab)
    {
        $this->tableauBillets = $tab;
    }

    /**
     * methode publique qui gere l'affichage general
     * si le parametre vaut 1, cela affiche les details d'un billet
     * si le parametre vaut 2, cela affiche une liste de billets
     */
    public function render($selecteur)
    {
        $d = \picof\dispatch\Dispatcher::getInstance();
        $r = $d->getRequest();
        $route = $r->getRouteInfo();

        $f = <<<END
        <!DOCTYPE html>
            <html>
                <head>
                    <meta charset="utf-8" />
                    <title>Les Gremlins montrent leur culture</title>
                    <link href="/~lucashaas/iutserver/projet/css/style.css" rel="stylesheet" type="text/css" />
                </head>

                <body>
                    <header>
                        <img src="/~lucashaas/iutserver/projet/css/ressources/gremlins_movie.jpg" alt="Image de Gremlins" id="logo" />
                        <p class="titre-page">Les Gremlins montrent leur culture</p>
END;
        $f=$f.$this->blocAuthentification();
        $f=$f."</header>";
        $f=$f."<section>";
        $f=$f. $this->genererBlocCategorie();
        $f=$f. $this->genererBlocBilletsRecents();
        if ($selecteur == 1) {
            $f=$f. $this->genererAffichageDetaille();
        } else if ($selecteur == 2) {
            $f=$f. $this->genererAffichageListe();
        } else if ($selecteur == 3) {
            $f=$f. $this->genererAffichageCategorie();
        }

        $f = $f.<<<END
                    </section>
                    <footer>
                        <ul id="liens-footer">
				            <li class="first">
END;
        $f=$f.                  "<a href=\"" . $route . "/index.php"."\">"."Accueil</a>";
        $f=$f.              "</li>";
        $f=$f.              "<li>";
        $f=$f.                  "<a href=\"" . $route . "/admin/panel"."\">"."Administration</a>";
        $f=$f.<<<END
				            </li>
			            </ul>
		                <p> &copy; 2014-2015 Lucas HAAS et Theodore LAMBOLEZ. Tous droits reserves.</p>
                    </footer>
                </body>
            </html>
END;

        print $f;
    }

    /**
     * methode privee qui genere le bloc de categorie
     */
    private function genererBlocCategorie() {
        $f = '<aside>';
        $f = $f.'<h1>Liste des categories</h1>';
        $d = \picof\dispatch\Dispatcher::getInstance();
        $r = $d->getRequest();
        $route = $r->getRouteInfo();
        $r = \blogapp\model\Categorie::all();
        $f=$f.'<ul>';
        foreach ($r as $v) {
            $f=$f. "<li><a href=\"" . $route . "/blog/cat?id=" . $v->id . "\">" . $v->titre . "</a></li>";
        }
        $f=$f.'</ul>';
        $f = $f."</aside>";
        return $f;
    }

    /**
     * methode privee qui genere le bloc des 10 billets les plus récents
     */
    private function genererBlocBilletsRecents() {
        $f = '<aside>';
        $f = $f.'<h1>Derniers articles</h1>';
        $d = \picof\dispatch\Dispatcher::getInstance();
        $r = $d->getRequest();
        $route = $r->getRouteInfo();
        $billets = \blogapp\model\Billet::orderBy('date', 'DESC')->take(10)->get();
        $f=$f.'<ul>';
        foreach ($billets as $v) {
            $f=$f. "<li><a href=\"" . $route . "/blog/billet?id=" . $v->id . "\">" . $v->titre . "</a></li>";
        }
        $f=$f.'</ul>';
        $f = $f."</aside>";
        return $f;
    }

    /**
     * methode qui genere l'affichage detaille d'un billet
     */
    public function genererAffichageDetaille() {
        $d = \picof\dispatch\Dispatcher::getInstance();
        $r = $d->getRequest();
        $route = $r->getRouteInfo();
        $f = '<article>';
        //$f=$f. "Id du billet : ".$this->tableauBillets[0]->id."</br>";
        $f=$f. '<h1>'. $this->tableauBillets[0]->titre;

        //convertion de la date en francais
        list($year, $month, $day) = explode("-", $this->tableauBillets[0]->date);
        $date_fr = "$day/$month/$year";

        $f=$f. '<em> redige le '.$date_fr."</em></h1>";
        $f=$f. '<p class="message-article">'.$this->tableauBillets[0]->body."</p>";
        $c = \blogapp\model\Categorie::find($this->tableauBillets[0]->cat_id);
        $f=$f. '<p class="categorie-article">'.'Categorie : '."<a href=\"".$route."/blog/cat?id=".$this->tableauBillets[0]->cat_id."\">".$c->titre."</a></p>";
        $f = $f."</article>";
        return $f;
    }

    // methode qui genere l'affichage de la liste des billets
    public function genererAffichageListe() {
        $f = "";
        $d = \picof\dispatch\Dispatcher::getInstance();
        $r = $d->getRequest();
        $route = $r->getRouteInfo();
        foreach ($this->tableauBillets as $v) {
            $f=$f."<article>";
            $f=$f.'<h1>'. $v->titre;

            //convertion de la date en francais
            list($year, $month, $day) = explode("-", $v->date);
            $date_fr = "$day/$month/$year";
            //$liste = explode("-", $v->date);
            //$date_fr = $liste[2]."/".$liste[1]."/".$liste[0];

            $f=$f. '<em> redige le '.$date_fr."</em></h1>";
            $f=$f. '<p class="message-article">'.substr($v->body, 0, 30)."...</p>";
            //$f=$f.'<p class="titre-article">'. "<a href=\"".$route."/blog/billet?id=".$v->id."\">". $v->titre."</a></br>";

            //Bouton lire la suite
            $f=$f.'<p class="lire-suite">'. "<a href=\"".$route."/blog/billet?id=".$v->id."\">"."Lire la suite"."</a></p>";


            $f=$f."</article>";
        }
        return $f;
    }

    // methode qui genere l'affichage de la liste des billets d'une categorie
    public function genererAffichageCategorie() {
        $f = "";
        $d = \picof\dispatch\Dispatcher::getInstance();
        $r = $d->getRequest();
        $route = $r->getRouteInfo();

        $b = \blogapp\model\Billet::all();
        foreach ($b as $v) {
            if ($v->cat_id == $this->tableauBillets[0]->id) {
                $f=$f."<article>";
                $f=$f.'<h1>'. $v->titre;

                //convertion de la date en francais
                list($year, $month, $day) = explode("-", $v->date);
                $date_fr = "$day/$month/$year";

                $f=$f. '<em> redige le '.$date_fr."</em></h1>";
                $f=$f. '<p class="message-article">'.substr($v->body, 0, 30)."...</p>";
                //$f=$f.'<p class="titre-article">'. "<a href=\"".$route."/blog/billet?id=".$v->id."\">". $v->titre."</a></br>";

                //Bouton lire la suite
                $f=$f.'<p class="lire-suite">'. "<a href=\"".$route."/blog/billet?id=".$v->id."\">"."Lire la suite"."</a></p>";


                $f=$f."</article>";
            }
        }

        return $f;
    }

    public function blocAuthentification() {
        $d = \picof\dispatch\Dispatcher::getInstance();
        $r = $d->getRequest();
        $route = $r->getRouteInfo();
        $f='<div class="authentification">';
        $f=$f.'<form id="fl" method="POST" action="'.$route. "/#" .'">';
        $f=$f.<<<END
                <p><label for="fl_nom">Nom d'utilisateur: </label>
                <input type="text" id="fl_nom" name="nom_login" required></p>
                <p><label for="fl_mdp">Mot de passe: </label>
                <input type="password" id="fl_mdp" name="mdp_login" required></p>
                <button style="cursor:pointer;" type="submit" name="valider_login" value="valid_fl">Connexion</button>
                </form>
END;
        $f=$f.'<button type="button" name="inscription" value="inscription">'."<a href=\"".$route."/admin/register/add"."\">"."Inscription</a></button>";
        $f=$f."</div>";
        return $f;
    }

}
