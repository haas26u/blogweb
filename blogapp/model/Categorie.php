<?php
/**
 * Created by PhpStorm.
 * User: lucashaas
 * Date: 09/12/14
 * Time: 11:37
 */

namespace blogapp\model;

class Categorie extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'categorie';
    protected $idColumn = 'id';
    public $timestamps = false;

} 