<?php
/**
 * Created by PhpStorm.
 * User: lucashaas
 * Date: 09/12/14
 * Time: 11:38
 */

namespace blogapp\model;

class Billet extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'billets';
    protected $idColumn = 'id';
    public $timestamps = false;

} 