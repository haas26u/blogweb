<?php
/**
 * Created by PhpStorm.
 * User: lucashaas
 * Date: 24/11/14
 * Time: 10:25
 */

namespace blogapp\control;
use blogapp\model\Billet;
use blogapp\model\Categorie;
use \picof\AbstractController;
use \picof\utils\HttpRequest;


class AdminController extends AbstractController {

    // correspond a la requete /admin/panel
    // acces au panneau d'administration ud blog
    public function accederPanel() {
        $vue = new \blogapp\vue\VueAdmin() ;
        $vue->render( 0 ) ;
    }

    // correspond a la requete /admin/billet/add
    // ajout d'un message : affichage du formulaire
    public function ajouterMessageBillet() {
        $vue = new \blogapp\vue\VueAdmin() ;
        $vue->render( 1 ) ;
    }

    // correspond a la requete /admin/billet/save
    // ajout d’un message : envoi des données
    public function envoyerDonneesBillet() {
        if (isset ($_POST['valider_creation']) && $_POST['valider_creation']=='valid_fcb') {
            //controle des valeurs
            $titre=$_POST['titre_article'];
            $titre_filtre=filter_var($titre,FILTER_SANITIZE_STRING);
            $corps=$_POST['corps_article'];
            $corps_filtre=filter_var($corps,FILTER_SANITIZE_STRING);
            $cat_titre=$_POST['select-categorie'];
            $cat=\blogapp\model\Categorie::where('titre','=',$cat_titre)->get();
            foreach ($cat as $v) {
                $cat_id= $v->id;
            }
            //creation de la requete sql
            $billet = new Billet();
            //$billet->id
            $billet->titre=$titre_filtre;
            $billet->body=$corps_filtre;
            $billet->cat_id=$cat_id;
            date_default_timezone_set('UTC');
            $billet->date=date("Y-m-d");
            $billet->save();

            $vue = new \blogapp\vue\VueAdmin() ;
            $vue->render( 3 ) ;
        }
    }

    // correspond a la requete /admin/cat/add
    // ajout d'une catégorie : affichage du formulaire
    public function ajouterCategorie() {
        $vue = new \blogapp\vue\VueAdmin() ;
        $vue->render( 2 ) ;
    }

    // correspond a la requete /admin/cat/save
    // ajout d'une catégorie : envoi des données
    public function envoyerDonneesCategorie() {
        if (isset ($_POST['valider_creation']) && $_POST['valider_creation']=='valid_fcc') {
            //controle des valeurs
            $titre=$_POST['titre_categorie'];
            $titre_filtre=filter_var($titre,FILTER_SANITIZE_STRING);
            $description=$_POST['description_categorie'];
            $description_filtre=filter_var($description,FILTER_SANITIZE_STRING);
            //creation de la requete sql
            $categorie = new Categorie();
            //$billet->id
            $categorie->titre=$titre_filtre;
            $categorie->description=$description_filtre;
            $categorie->save();

            $vue = new \blogapp\vue\VueAdmin() ;
            $vue->render( 4 ) ;
        }
    }

    // correspond a la requete /admin/register/add
    public function ajouterUtilisateur() {
        $vue = new \blogapp\vue\VueAdmin() ;
        $vue->render( 5 ) ;
    }

    // correspond a la requete /admin/register/save
    public function creerUtilisateur() {
        $u=$_POST['nom_utilisateur'];
        $p=$_POST['mdp_utilisateur'];
        try {
            Authentification::createUser($u,$p) ;
        } catch (AuthException $ae) {
            echo "bad login name or password<br>";
        }
    }

    // correspond a la requete /admin/billet/remove/list
    public function supprimerBillet() {
        $vue = new \blogapp\vue\VueAdmin() ;
        $vue->render( 6 ) ;
    }

    // correspond a la requete /admin/billet/remove/save
    public function supprimerDonneesBillet() {
        if (isset ($_POST['valider_choix']) && $_POST['valider_choix']=='valid_fsb') {
            foreach($_POST['billet'] as $valeur) {
                $billet = Billet::find($valeur);
                $billet->delete();
            }
            $vue = new \blogapp\vue\VueAdmin() ;
            $vue->render( 7 ) ;
        }
    }

    // correspond a la requete /admin/cat/remove/list
    public function supprimerCategorie() {
        $vue = new \blogapp\vue\VueAdmin() ;
        $vue->render( 8 ) ;
    }

    // correspond a la requete /admin/cat/remove/save
    public function supprimerDonneesCategorie() {
        if (isset ($_POST['valider_choix']) && $_POST['valider_choix']=='valid_fsc') {
            foreach($_POST['categorie'] as $valeur) {
                $categorie = Categorie::find($valeur);
                $categorie->delete();
            }
            $vue = new \blogapp\vue\VueAdmin() ;
            $vue->render( 9 ) ;
        }
    }

} 