<?php
/**
 * Created by PhpStorm.
 * User: lucashaas
 * Date: 19/11/14
 * Time: 09:33
 */

namespace blogapp\control;
use \picof\AbstractController;
use \picof\utils\HttpRequest;

class BlogController extends AbstractController {

    // correspond a la requete /blog/list
    public function listerBillets() {
        $list = \blogapp\model\Billet::orderBy('date', 'DESC')->get();
        $vue = new \blogapp\vue\VueBlog( $list ) ;
        $vue->render( 2 ) ;
    }

    // correspond a la requete /blog/billet?id=n
    public function afficherBillet() {
        $tab = $this->httpreq->get;
        $id=$tab['id'];
        $billet = \blogapp\model\Billet::find($id) ;
        $vue = new \blogapp\vue\VueBlog( array( $billet ) ) ;
        $vue->render( 1 ) ;
    }

    // correspond a la requete /blog/liscat
    public function listerCategories() {
        $route=$this->httpreq->getRouteInfo();
        $r = \blogapp\model\Categorie::all();
        foreach ($r as $v) {
            print "<a href=\"" . $route . "/blog/cat?id=" . $v->id . "\">" . $v->titre . "</a></br>";
        }
    }

    // correspond a la requete /blog/cat?id=n
    public function listerBilletsCategorie() {
        $id=$this->httpreq->get['id'];
        $categorie = \blogapp\model\Categorie::find($id);
        $vue = new \blogapp\vue\VueBlog( array( $categorie ) ) ;
        $vue->render( 3 ) ;
    }

}