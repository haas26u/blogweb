<?php
/**
 * Created by PhpStorm.
 * User: lucashaas
 * Date: 09/12/14
 * Time: 11:12
 */

namespace conf;
use Illuminate\Database\Capsule\Manager as DB;

class ConfEloquent {

    public static function configurer($config) {
        $conf = parse_ini_file($config);

        $db = new DB();
        $db->addConnection($conf);
        $db->setAsGlobal();
        $db->bootEloquent();
    }

} 