<?php
/**
 * Created by PhpStorm.
 * User: lucashaas
 * Date: 19/11/14
 * Time: 09:24
 */

function myAutoload($classname) {
    //echo $classname; echo '</br>';
    $filename='';
    $classname=ltrim($classname,'\\');
    $filename=str_replace('\\',DIRECTORY_SEPARATOR,$classname);
    $filename.='.php';

    //echo $filename;
    require_once $filename;
}


spl_autoload_register('myAutoload');