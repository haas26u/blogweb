<?php
/**
 * Created by PhpStorm.
 * User: lucashaas
 * Date: 19/11/14
 * Time: 09:40
 */

namespace picof\dispatch;
use \picof\utils\HttpRequest;


class Dispatcher {

    private $routes;
    private $request;
    private static $disp= null;

    public function __construct(HttpRequest $h) {
        $this->routes = array();
        $this->request = $h;
        self::$disp = $this;
    }

    public function addRoute($route, $controleur, $methode) {
        $this->routes[$route] = array('c'=>$controleur, 'm'=>$methode);
    }

    public function dispatch() {
        $path = $this->request->getPathInfo();
        #echo $path.'</br>';
        foreach ($this->routes as $route=>$tab) {
            #echo $route.'</br>';
            $pos = strpos($path,$route);
            #echo $pos.'</br>';
            if ($pos === false) {
                # $route n'est pas inclus dans $path
            } else {
                $controller = $tab[ 'c' ];
                #echo $controller.'</br>';
                $method = $tab[ 'm' ];
                #echo $method.'</br>';
                $c = new $controller( $this->request );
                $c->$method();
            }
        }
    }

    public static function getInstance(){
        if(is_null(self::$disp))
            return null;
        return self::$disp;
    }

    public function getRequest(){
        if(is_null($this->request))
            return null;
        return $this->request;
    }

} 