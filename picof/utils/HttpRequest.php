<?php
/**
 * Created by PhpStorm.
 * User: lucashaas
 * Date: 19/11/14
 * Time: 08:29
 */

namespace picof\utils;

class HttpRequest {

    private $method;
    private $script_name;
    private $request_uri;
    private $query;
    private $get;

    public function __construct() {
        # $method : contient la méthode HTTP (GET, POST...)
        # $script_name : contient le nom du script appelé, avec le chemin (voir $_SERVER['SCRIP_NAME'])
        # $request_uri : contient l'uri complète de la requête (voir $_SERVER['SCRIP_NAME'])
        # $query : contient la partie query de la requêtes
        # $get : contient le tableau des paramètres de la requête ($_GET)

        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->script_name = $_SERVER['SCRIPT_NAME'];
        $this->request_uri = $_SERVER['REQUEST_URI'];
        $this->query = $_SERVER['QUERY_STRING'];
        $this->get = $_GET;
    }

    public function __get( $attname ) {
        if ( property_exists ($this, $attname) ) {
            return $this->$attname ; }
        else throw new \Exception("invalid Property") ;
    }

    public function __set( $attname, $attrval ) {
        if (property_exists($this, $attname)) {
            $this->$attname = $attrval;
            return $this->$attname;
        } else throw new \Exception("invalid Property");
    }

    public function getPathInfo() {
        $s = dirname($this->script_name);
        return str_replace($s,"",$this->request_uri);
        #$s = dirname($_SERVER['SCRIPT_NAME']);
        #str_replace($s,"",$_SERVER['REQUEST_URI
    }

    public function getRouteInfo() {
        return dirname($this->script_name);
    }
} 