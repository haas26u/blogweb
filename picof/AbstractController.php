<?php
/**
 * Created by PhpStorm.
 * User: lucashaas
 * Date: 24/11/14
 * Time: 10:27
 */

namespace picof;
use \picof\utils\HttpRequest;

abstract class AbstractController {

    protected $httpreq;

    public function __construct(HttpRequest $h) {
        $this->httpreq=$h;
    }

} 